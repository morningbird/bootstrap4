<?php

namespace morningbird\bootstrap4;

class ActiveField extends \yii\widgets\ActiveField {
    
    public $hintOptions = [
        'class' => 'form-text text-muted',
        'tag' => 'small'
    ];
    
    public $errorOptions = [
        'class' => 'invalid-feedback'
    ];
    public function dateInput($options = array()) {
        $view = \Yii::$app->getView();
        
        //register JS untuk datetimepicker
        \frontend\assets\Datetimepicker::register($view);
        $myID = Html::getInputId($this->model, $this->attribute);
        $defaultFormat = \Yii::$app->params['defaultDateFormat'];
        \Yii::$app->getView()->registerJs("
            $('#{$myID}').datetimepicker({
                format: '{$defaultFormat}'
                });
            ");
        
        //tambahkan class datepicker sebagai penanda
        if(!isset($options['class']))
        {
            $options['class'] = 'form-control datepicker';
        }
        else {
            $options['class'] .= ' datepicker';
        }
                
        return parent::textInput($options);
    }
}
