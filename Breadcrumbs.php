<?php

namespace morningbird\bootstrap4;

class Breadcrumbs extends \yii\widgets\Breadcrumbs {
    public $itemTemplate = "<li class='breadcrumb-item'>{link}</li>\n";
}
