<?php

namespace morningbird\bootstrap4;

class ActiveForm extends \yii\widgets\ActiveForm {
    public $fieldClass = '\morningbird\bootstrap4\ActiveField';
}
