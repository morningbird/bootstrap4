<?php
namespace morningbird\bootstrap4;

class MDActiveForm extends \yii\widgets\ActiveForm {
    public $fieldClass = '\morningbird\bootstrap4\MDActiveField';
    
    public function field($model, $attribute, $options = array()) {
        return parent::field($model, $attribute, $options);
    }
}
