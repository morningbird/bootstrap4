<?php

namespace morningbird\bootstrap4;

use yii\helpers\Html;

class MDActiveField extends \yii\widgets\ActiveField {
    public $template = "{label}\n{input}\n{hint}\n{error}";
    
    public function textarea($options = array()) {
        if(!isset($options['class']))
        {
            $options['class'] = 'form-control';
        }
        
        return parent::textarea($options);
    }
    
    public function checkboxListVertical($items, $options = array()) {
        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $s = '';
        $s .= Html::hiddenInput(Html::getInputName($this->model, $this->attribute));
        foreach($items as $key => $item)
        {
            $myCheckbox = Html::activeCheckbox($this->model, $this->attribute . '[]', [
                'label' => $item,
                'value' => $key,
                'uncheck' => false
            ]);
            
            $myCheckbox = Html::tag('div', $myCheckbox, [
                'class' => 'mt-2'
            ]);
            
            $s .= $myCheckbox;
        }
        $this->parts['{input}'] = $s;

        return $this; 
    }
    
}
